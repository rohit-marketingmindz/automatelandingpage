<!DOCTYPE html>
<html lang=en>
<head> 
    <meta charset=utf-8>
    <meta name=viewport content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="MVP Development for Startups - MVP Apps and Websites">
    <meta name=description content="MVP development services for Startups. Grow your startup business with our top MVP apps, custom software and website development solutions for startups.">
    <title>MVP development for Startups</title>
    <link rel="icon" href="" />
    <link href=css/bootstrap.min.css rel=stylesheet>
    <link href=css/style.css rel=stylesheet>
    <!--[if lt IE 9]>
<script src=js/html5shiv.js></script>
<script src=js/respond.min.js></script>
<![endif]-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel=stylesheet type=text/css>

    <!-- font-Awesome  -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <!--<script src="js/jquery.min.js"></script>-->
    <!-- <script src="js/dinesh.js"></script>-->
    <!--<script src=js/jquery-ui.min.js></script>-->
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NLWPK6Q');</script>
<!-- End Google Tag Manager -->
    <script type=text/javascript>
    $(document).ready(function() {
        var b = $(".testimonial-wrapper .testimonial-item");
        var c = b.length;
        var a = 0;
        b.hide();
        setTimeout(function() {
            d(a)
        }, 1000);

        function d(e) {
            b.eq(e).fadeIn("slow", function() {
                if (e <= c) {
                    setTimeout(function() {
                        b.fadeOut("slow", function() {
                            if (e == c) {
                                e = 0
                            } else {
                                e++
                            }
                            setTimeout(function() {
                                d(e)
                            }, 500)
                        })
                    }, 2000)
                }
            })
        }
    });
    </script>
    <script>
    ! function(h, a, i, c, j, d, g) {
        if (h.fbq) {
            return
        }
        j = h.fbq = function() {
            j.callMethod ? j.callMethod.apply(j, arguments) : j.queue.push(arguments)
        };
        if (!h._fbq) {
            h._fbq = j
        }
        j.push = j;
        j.loaded = !0;
        j.version = "2.0";
        j.queue = [];
        d = a.createElement(i);
        d.async = !0;
        d.src = c;
        g = a.getElementsByTagName(i)[0];
        g.parentNode.insertBefore(d, g)
    }(window, document, "script", "https://connect.facebook.net/en_US/fbevents.js");
    fbq("init", "2008771289347298");
    fbq("track", "PageView");
    </script>
    <noscript><img height=1 width=1 style=display:none src="https://www.facebook.com/tr?id=2008771289347298&ev=PageView&noscript=1" /></noscript>
    <!--// CLOSE HEAD //-->
    <!-- Below is the Bing Ads UET tag tracking code. You need to add it to your entire website in the header or body sections. If your website has a master page, then you can add it once there and it is included in all pages.
For tips see http://help.bingads.microsoft.com/#apex/3/en/56688/-1 -->
    <script>
    (function(w, d, t, r, u) {
        var f, n, i;
        w[u] = w[u] || [], f = function() {
            var o = {
                ti: "5602031"
            };
            o.q = w[u], w[u] = new UET(o), w[u].push("pageLoad")
        }, n = d.createElement(t), n.src = r, n.async = 1, n.onload = n.onreadystatechange = function() {
            var s = this.readyState;
            s && s !== "loaded" && s !== "complete" || (f(), n.onload = n.onreadystatechange = null)
        }, i = d.getElementsByTagName(t)[0], i.parentNode.insertBefore(n, i)
    })(window, document, "script", "//bat.bing.com/bat.js", "uetq");
    </script>
    <noscript><img src="//bat.bing.com/action/0?ti=5602031&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>


</head>
<body class=light>

<!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 999459375;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/999459375/?guid=ON&amp;script=0"/>
</div>
</noscript>
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NLWPK6Q" 
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<!-- Google Code for MVP Lead Tracking Conversion Page
In your html page, add the snippet and call
goog_report_conversion when someone clicks on the
chosen link or button. -->
<script type="text/javascript">
  /* <![CDATA[ */
  goog_snippet_vars = function() {
    var w = window;
    w.google_conversion_id = 999459375;
    w.google_conversion_label = "tps0CJKY92kQr5TK3AM";
    w.google_remarketing_only = false;
  }
  // DO NOT CHANGE THE CODE BELOW.
  goog_report_conversion = function(url) {
    goog_snippet_vars();
    window.google_conversion_format = "3";
    var opt = new Object();
    opt.onload_callback = function() {
    if (typeof(url) != 'undefined') {
      window.location = url;
    }
  }
  var conv_handler = window['google_trackConversion'];
  if (typeof(conv_handler) == 'function') {
    conv_handler(opt);
  }
}
/* ]]> */
</script>
<script type="text/javascript"
  src="//www.googleadservices.com/pagead/conversion_async.js">
</script>


    <main id=top class=masthead role=main>
        <div class=container>
            <div class="row">
                <div class="col-md-2 col-sm-2 tm_logo">
                    <img src="images/tm-logo1.png" alt="" />
                </div>
                <div class="col-md-10 col-sm-10 tm-cantent">
                    <h1 class=main-title>Build A Mobile App MVP - $5000 - 100% Money Back</h1>
                </div>
                <div class="col-xs-12">
                	<div class="call-now-button">
				        <a href="tel:+1 6502649669" class="btn btn-success btn-block"><i class="fa fa-phone phone" aria-hidden="true"></i>
				 Call now</a>
    				</div>
                </div>
            </div>
            <div class="row has-padding-top">
                <div class="col-sm-12 col-md-8 dream-img">
                   <img src=images/real-est-banner.png alt="Build Minimum Viable Product" class="img-responsive mm_desktop_img">
                    <img src=images/real-est-banner.png alt="Build Minimum Viable Product" class="img-responsive mm_mobile_img">
                </div> 



                <div class="col-sm-6 col-md-4 mm-form" id="Contactformbuild-mvp">
                    <div class="contact-form">

                        <h6 class=mini-title>Get your FREE scope and wireframe today!</h6>
                       
            
           

                       <!-- Zoho Form Source code -->
                       
                       
                                     <!-- Note :
   - You can modify the font style and form style to suit your website. 
   - Code lines with comments ���Do not remove this code���  are required for the form to work properly, make sure that you do not remove these lines of code. 
   - The Mandatory check script can modified as to suit your business needs. 
   - It is important that you test the modified form before going live.-->
<div id='crmWebToEntityForm'>
   <META HTTP-EQUIV ='content-type' CONTENT='text/html;charset=UTF-8'>
   <form action='https://crm.zoho.com/crm/WebToLeadForm' name=WebToLeads2677293000000351003 method='POST' onSubmit='javascript:document.charset="UTF-8"; return checkMandatory()' accept-charset='UTF-8'>

	 <!-- Do not remove this code. -->
	<input type='text' style='display:none;' name='xnQsjsdp' value='170052dace33757371cc54836f5813886b5d2ff36a0af5ddaa8a081d70f969fd'/>
	<input type='hidden' name='zc_gad' id='zc_gad' value=''/>
	<input type='text' style='display:none;' name='xmIwtLD' value='617770d327d74458e6f2eb42ef07f2a4e2bc657acd4b60442ad58a2211d80fdd'/>
	<input type='text' style='display:none;'  name='actionType' value='TGVhZHM='/>

	<input type='text' style='display:none;' name='returnURL' value='https&#x3a;&#x2f;&#x2f;www.mvpdevelopment.company&#x2f;thank-you&#x2f;' /> 
	 <!-- Do not remove this code. -->

	<style>
		tr , td { 
			padding:6px;
			border-spacing:0px;
			border-width:0px;
			}
	</style>


	<form class=form id="phpcontactform" data-redirect=none>
                                            
            <div class=form-group>
                <input class="form-control 
Editing:  
/home/devmindz/public_html/buildmvp/index.php
 Encoding:       
 Keyboard shortcuts
input-lg" type=text placeholder="Full Name*" name='Last Name' id=Last Name required>
            </div>
            
            <div class=form-group>
                <input class="form-control input-lg" type=email placeholder="Email ID*" name='Email' id=Email required>
            </div>
            <div class=form-group>
                <input class="form-control input-lg" type=text placeholder=Phone* name='Phone' id=Phone maxlength=10 required onkeyup="this.value=this.value.replace(/[^\d]/,'')">
            </div>
            <div class=form-group>
                <textarea class="form-control input-lg textarea_mm" type=text placeholder="Message" name='Description' id=Description></textarea>
            </div>
           
            
             <input type=hidden name ="lead_source" id ="lead_source" value="hello">
            <input class="btn btn-success btn-lg btn-block" type=submit value='Get a Free Consultation Today'>
            <span class=loading></span>
        </form>
	<script>
 	  var mndFileds=new Array('Last Name','Phone','Email');
 	  var fldLangVal=new Array('Full Name','Phone','Email');
		var name='';
		var email='';

 	  function checkMandatory() {
		for(i=0;i<mndFileds.length;i++) {
		  var fieldObj=document.forms['WebToLeads2677293000000351003'][mndFileds[i]];
		  if(fieldObj) {
			if (((fieldObj.value).replace(/^\s+|\s+$/g, '')).length==0) {
			 if(fieldObj.type =='file')
				{ 
				 alert('Please select a file to upload.'); 
				 fieldObj.focus(); 
				 return false;
				} 
			alert(fldLangVal[i] +' cannot be empty.'); 
   	   	  	  fieldObj.focus();
   	   	  	  return false;
			}  else if(fieldObj.nodeName=='SELECT') {
  	   	   	 if(fieldObj.options[fieldObj.selectedIndex].value=='-None-') {
				alert(fldLangVal[i] +' cannot be none.'); 
				fieldObj.focus();
				return false;
			   }
			} else if(fieldObj.type =='checkbox'){
 	 	 	 if(fieldObj.checked == false){
				alert('Please accept  '+fldLangVal[i]);
				fieldObj.focus();
				return false;
			   } 
			 } 
			 try {
			     if(fieldObj.name == 'Last Name') {
				name = fieldObj.value;
 	 	 	    }
			} catch (e) {}
		    }
		}
	     }
	   
</script>
	</form>
</div>

                       
                    </div>
                </div>
            </div>
        </div>
    </main>

    <div class="services" id="service1">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12">
                    <div class="section-head">
                        <h2>We Focus Our Attention On These Areas</h2>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="single-service animate_fade_in">
                        <div class="row">
                            <div class="col-xs-3 text-center half-gutter">
                                <img src="images/web_ico_header.svg" alt="Build Web based MVP">
                            </div>
                            <div class="col-xs-9 half-gutter">
                                <h3>Web</h3>
                                <p>We offer design and development services of sophisticated web-based solutions with continuous support.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="single-service animate_fade_in">
                        <div class="row">
                            <div class="col-xs-3 text-center half-gutter">
                                <img src="images/mobile_ico_header.svg" alt="MVP App Development">
                            </div>
                            <div class="col-xs-9 half-gutter">
                                <h3>Mobile</h3>
                                <p>Our specialized teams use design-and-change-driven agile approach to deliver supreme quality. We make your brand more visible to your customers.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="single-service animate_fade_in">
                        <div class="row">
                            <div class="col-xs-3 text-center half-gutter">
                                <img src="images/wearables_ico_header.svg" alt="Wearables App MVP Development">
                            </div>
                            <div class="col-xs-9 half-gutter">
                                <h3>Wearables</h3>
                                <p>Our priority has always been to explore and foresee the trends of the market. We offer a complete package of services and skills to create your wearables app.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="single-service animate_fade_in">
                        <div class="row">
                            <div class="col-xs-3 text-center half-gutter">
                                <img src="images/cloud_ico_header.svg" alt="Cloud based MVP development">
                            </div>
                            <div class="col-xs-9 half-gutter">
                                <h3>Cloud</h3>
                                <p>We offer a complete package of services and skills to develop comprehensive cloud solutions.</br>
                                    </br>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="single-service animate_fade_in">
                        <div class="row">
                            <div class="col-xs-3 text-center half-gutter">
                                <img src="images/iot_ico_header.svg" alt="IoT (Internet of Things) MVP Development">
                            </div>
                            <div class="col-xs-9 half-gutter">
                                <h3>Internet Of Things (IOT)</h3>
                                <p>We offer a complete package of skills and services to make things smarter! We can prototype your very own IoT device or smart sensor.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="single-service animate_fade_in">
                        <div class="row">
                            <div class="col-xs-3 text-center half-gutter">
                                <img src="images/reality_ico_header.svg" alt="Build MVP for Virtual reality">
                            </div>
                            <div class="col-xs-9 half-gutter">
                                <h3>Virtual Reality</h3>
                                <p>Using our change-driven agile approach you can get valuable results with supreme quality, quick results and continuous feedback.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container" id="mm-price-table">
        <div class="row">
            <div class="col-md-12">
                <br>
                <!-- Plans -->
                <section id="plans">
                    <div class="row">
                        <div class="col-md-3 col-sm-6 plans-toll">
                            <div id="pricing-table-a" class="clear pricing-table1">
                                <div class="plan tip1" id="pricing-table1">
                                    <h3>Wireframe Design <span class="shine tooltip">?
                                        <span class="tooltiptext">Wireframes are simple black and white layouts that outline the specific size and placement of page elements, site features, conversion areas and navigation for your mobile app or website</span></span><span class="price-N1">$1500</span></h3>
                                    <p>Select this service if you’re on early stage of product idea development.
                                    </p>
                                    <p>Includes:</p>
                                    <ul>
                                        <li>Understand your requirements</li>
                                        <li>Research and generate design ideas</li>
                                        <li>Design Wireframe to identify CTA’s and other important information</li>
                                        <li>Duration – 1 Week</li>
                                        <li>Average price</li>
                                    </ul>
                                    <a class="signup" href="#Contactformbuild-mvp">free consultation</a>
                                </div>
                                ...
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 plans-toll">
                            <div id="pricing-table-b" class="clear pricing-table1">
                                <div class="plan tip2" id="pricing-table2">
                                    <h3>Prototype Development  <span class="shine tooltip"> ?
                                        <span class="tooltiptext"> Website prototypes are interactive demos of a website. These are often used to gather feedback from project stakeholders early in the project lifecycle, before the project goes into final development</span></span> 
                                        <span class="price-N1">$2500</span></h3>
                                    <p>Select this service if you’re ready with your idea and would like to present effectively in front of stakeholders.
                                    </p>
                                    <p>Includes everything in Wireframe design service plus:</p>
                                    <ul>
                                        <li>Understand your idea</li>
                                        <li>Prepare requirements</li>
                                        <li>Convert your idea from sketch to actual web / mobile design</li>
                                        <li>Develop clickable prototype to test version of a product to be developed – which you can touch, feel, and see.</li>
                                        <li>Duration – 2 weeks</li>
                                        <li>Average price</li>
                                    </ul>
                                    <a class="signup" href="#Contactformbuild-mvp">free consultation</a>
                                </div>
                                ...
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 plans-toll">
                            <div id="pricing-table-c" class="clear pricing-table1">
                                <div class="plan tip3" id="pricing-table3">
                                    <h3>MVP Development <span class="shine tooltip"> ?
                                         <span class="tooltiptext"> A minimum viable product (MVP) is a product with just enough features to satisfy early customers, and to provide feedback for future development. </span></span> <span class="price-N1">$5000</span></h3>
                                    <p>Select this service if you’ve limited funds and are finding right balance between minimal design and maximum value.
                                    </p>
                                    <p>Includes everything in Prototype development service plus:</p>
                                    <ul>
                                        <li>Understand your idea</li>
                                        <li>Gather requirements and document a scope of work </li>
                                        <li>Technical analysis and suggest a best technology for your MVP</li>
                                        <li>Assemble each feature to fit the scope</li>
                                        <li>Conduct a thorough audit before sending you a link and testing instructions</li>
                                        <li>Fix bugs and issues</li>
                                        <li>Launch your MVP</li>
                                        <li>Changes post launching as per early users’ feedback</li>
                                        <li>Duration – 4-5 weeks</li>
                                        <li>Average price</li>
                                    </ul>
                                    <a class="signup" href="#Contactformbuild-mvp">free consultation</a>
                                </div>
                                ...
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 plans-toll">
                            <div id="pricing-table-d" class="clear pricing-table1">
                                <div class="plan tip4" id="pricing-table4">
                                    <h3>Product Development <span class="shine tooltip">?
                                         <span class="tooltiptext"> >Product development, also called new product management, is a series of steps that includes the conceptualization, design, development and marketing of newly created or newly rebranded goods or services.</span></span><span class="price-N1">Call us</span></h3>
                                    <p>Select this service if you’re ready to develop a final product.
                                    </p>
                                    <p>Includes everything in MVP development service plus:</p>
                                    <ul>
                                        <li>Understand your product idea</li>
                                        <li>Technical analysis and suggest a best technology for your product</li>
                                        <li>Design as per wireframe layout</li>
                                        <li>Convert Design into web or mobile app</li>
                                        <li>Complete customization to meet exact feature and design needs</li>
                                        <li>Conduct a thorough audit before sending you a link and testing instructions</li>
                                        <li>Fix bugs and issues</li>
                                        <li>Beta Product Launch</li>
                                        <li>User feedback loop</li>
                                        <li>Final product launch</li>
                                        <li>Ongoing maintenance</li>
                                    </ul>
                                    <a class="signup" href="#Contactformbuild-mvp">free consultation</a>
                                </div>
                                ...
                            </div>
                        </div>
                    </div>
                </section>
                <!-- /Plans -->
            </div>
        </div>
    </div>
    <div class=container id=explore>
        <!--<div class="row has-padding-top">
            <div class="col-md-3 boy-img"> <img src=images/boy.jpg alt=image class="img-responsive center-block"> </div>
            <div class="col-md-9 mm-content">
                <div class=quick-features>
                    <div class=featues-single> <img src=images/icon-color-2.png alt="icon 01" class=pull-left />
                        <h5>MVP Development for Mobile App</h5>
                        <p>Experts at Technology Mindz develop MVP Mobile Apps and convert your idea into reality. We make your brand more visible to your customers.</p>
                    </div>
                    <div class=featues-single> <img src=images/icon-color-1.png alt="icon 01" class=pull-left />
                        <h5>MVP development for Website</h5>
                        <p>We build website MVP with extreme focus on building optimal solution to solve a primary problem.</p>
                    </div>
                    <div class=featues-single> <img src=images/icon-color-3.png alt="icon 01" class=pull-left />
                        <h5>MVP Custom Software Development</h5>
                        <p>MVP custom software development involves designing specific programs to solve specific business problems . Our business and technical experts will collaborateto build your MVP and develop a first prototype.</p>
                    </div>
                </div>
            </div>
        </div>-->
        <section class="row features has-padding-top">
            <div class="col-sm-6 col-md-4">
                <div class=thumbnail> <img src=images/placeholder1.png alt="MVP Development Comapny">
                    <div class=caption>
                        <h3>We Love Ideas</h3>
                        <p>Working with new ideas and start-ups is our passion. Not work.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class=thumbnail> <img src=images/Idea-to-product.png alt="Build MVP to convert your Idea into future">
                    <div class=caption>
                        <h3>Idea to Product</h3>
                        <p>We build an awesome, usable product around your idea.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class=thumbnail> <img src=images/we-support-maintain.png alt="Build MVP and get free support">
                    <div class=caption>
                        <h3>We Support & Maintain</h3>
                        <p>We make sure your product is up and running. You focus on demos and the path to world domination.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class=thumbnail> <img src=images/Obsessed-with-Quality.png alt=analytics-icon>
                    <div class=caption>
                        <h3>Obsessed with Quality</h3>
                        <p>We are fanatics about frameworks and standards. Result is high quality product at lower cost faster.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class=thumbnail> <img src=images/Mentorship.png alt="Free MVP Consultation">
                    <div class=caption>
                        <h3>Technical Advisor</h3>
                        <p>As a technical advisor, we discuss detailed information with the customer and advice project team to design innovative and environmentally sensitive project.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class=thumbnail> <img src=images/Affordable.png alt="Low cost MVP development">
                    <div class=caption>
                        <h3>Most Affordable</h3>
                        <p>You don't have to break the bank to purse your idea. No need to give up equity or complicated paperwork.</p>
                    </div>
                </div>
            </div>
        </section>
        <div class=section-title>
            <h2>Steps We Follow to Build a Low Cost MVP</h2>
        </div>
        <div class="video-block text-center" id=videoBlock>
           <img src=images/dream2_new.jpg  alt="Build MVP, MVP App">
        </div>
    </div>
  


<div class="container">
    <div class="row background text-center">
       <div class="col-md-6 android_ios one">
           <h2>Android</h2>
          <a href="https://play.google.com/store/apps/details?id=com.ionicframework.sftraining" target="_blank"> <img src="images/android-app.png" class="img-mobile" width="200" height="400" alt="MVP App development Android"></a>
           <br>
           <p><strong>SF Training : </strong>This application provides several types of free or paid exercises. Personally trained by yourself day by day via short videos and hire a coach. In this application you will get fitness training via coach and video.</p>
       </div>
       
       
        <div class="col-md-6 android_ios">
           <h2>iOS</h2>
            <a href="https://itunes.apple.com/us/app/practical-accounts-training/id1165287587?mt=8" target="_blank"><img src="images/ios-app.png" class="img-mobile" width="200" height="400" alt="MVP App Development iOS"></a>
           <br>
           <p><strong>Practical Accounts Training : </strong>Accounts practice on the GO. You can use mobile practical accounts, the free or paid version. Upgrade your accounts qualifications, during or after accounting course. Get experience, a competitive advantage in job hunting.</p>
       </div>
    </div>  
    
</div>

<div class="container_desktop">
<div class="container">
     <div class="row gray_color text-center">
        <div class="col-md-12 col-sm-12 mm-black">
          <img src="images/screen-1515324.png" class="img-mobile" width="70%" height="auto" alt="Web based MVP Development"> 
            <p><strong>Opt In Savings : </strong>Opt In Savings assist in marketing and promoting coupon/s to all groups through additional intelligent marketing mediums targeting the groups select for audiences. Customers can save select coupons on smart phones, tablets or computers in the cloud.</p>

        </div>
    </div>
    </div>
      <div class="highlight testimonials testimonial-wrapper">
        <div class="container testimonial-item" style=display:block;opacity:1>
            <div class=text-center>
                <img src=images/quote-icon.png alt=quote class=has-padding-top>
            </div>
            <section class="row has-margin-bottom">
                <div class="col-md-8 col-md-offset-2">
                    <blockquote>"I have used the services of Technology Mindz several times now. I am always tremendously pleased with their work and performance of the task. I highly recommend them as experienced and valuable assistance in your business needs."</blockquote>
                    <div class=clientblock>
                        <img src=images/paula-vail.jpg alt=.>
                        <p><strong>Paula Vail</strong>
                            <br> CEO &amp; Founder of Wellness Inspired</p>
                    </div>
                </div>
            </section>
        </div>
        <div class="container testimonial-item" style=display:block;opacity:1>
            <div class=text-center>
                <img src=images/quote-icon.png alt=quote class=has-padding-top>
            </div>
            <section class="row has-margin-bottom">
                <div class="col-md-8 col-md-offset-2">
                    <blockquote>It has been a delight working with Technology Mindz. They have been very professional and supportive from start to finish. They delivered all that was agreed, and even more. They dealt with change in requirements patiently and learned concepts of accounting (a new function to them) to finally deliver a great product. I just want to say thank you to the whole group who worked on the project and would recommend them to anyone.</blockquote>
                    <div class=clientblock>
                        <img src=images/BAKALUBA.jpg alt=.>
                        <p><strong>BAKALUBA MOSES</strong>
                            <br> CEO &amp; Founder of APA Accounting</p>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
    <div class=container>
        <div class=section-title>
            <h2>Frequently Asked Questions</h2>
            <h4>Got questions? We have got answers</h4>
        </div>
        <section class="row faq breath">
            <div class=col-md-6>
                <h6>What is The Lean Startup, and The Lean Methodology?</h6>
                <p>What started off as a book by Eric Ries has grown into a startup movement. With self organizing meetups in 94 Cities & 17 Countries. It’s become a standard for startups to get their products online first as bare-bones prototypes and immediately begin testing them. It’s now part of the Harvard Business School curriculum along with many other colleges and universities. See <a href=https://TheLeanStartup.com>TheLeanStartup.com</a> and the <a href="https://en.wikipedia.org/wiki/Lean_startup
                ">Wikipedia article</a> on The Lean Methodology.</p>
                <h6>How much does it cost?</h6>
                <p>We charge a mixture of equity and cash. This allows us to maintain an ongoing interest in the project’s success while also keeping the cost of the MVP down. Our MVP rate is $20 per hour and we will typically work 40 hours per week of work for 6 weeks. This includes a mixture of developer, designer and product/project management time. On an average you can expect to spend $4000.</p>
            </div>
            <div class=col-md-6>
                <h6>Why don’t I just hire a freelancer?</h6>
                <p>Freelancers will build exactly what you tell them to and you must tell it to them in an exact way. If you do not have your idea completely documented, much time will be spent communicating the idea to the freelancer along with communicating changes/tweaks as the product is being built.</p>
                <h6>What programming languages will my MVP be built with?</h6>
                <p>Most MVPs that we build use PHP, Ruby on Rails and/or Objective C. We work with you to find the best fit for your MVP and your team.</p>
                <h6>How long does it take?</h6>
                <p>We strive to build the MVP within 6 weeks. We will work with you to shape your MVP into something that can be built within this timeframe.</p>
            </div>
        </section>
    </div>
    <section class=highlight>
        <div class=container>
            <div class=section-title>
                <h2>Are you ready?</h2>
                <h4>Your product must solve at least one real problem, for one real audience, in one unique way.</h4>
            </div>
            <div class="row has-margin-bottom">
                <div class="col-md-12 text-center"> <a href=#top class="btn btn-primary btn-lg">REQUEST QUOTE</a> </div>
            </div>
        </div>
    </section>
    <!-- pop up code -->
    <!-- Modal -->
    <!--<div class="modal fade" id="myModal" role="dialog" ">
        <div class="modal-dialog MM-popup contact-form1">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body mm-pop-body">
                    <!-- <h2>Get a Free Consultation Now</h2> 
                    <h2>Download FREE Whitepapers</h2>
                    <div id="errors1"></div>
                    <div class="form-group">
                        <label for="popName">Name</label>
                        <input type="text" class="form-control" id="popName" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPhone">Phone No.</label>
                        <input type="tel" class="form-control" id="exampleInputPhone" placeholder="Phone No" min="1">
                    </div>
                    
                    <div> <span class="loading1"></span></div>
                    <div class="form-group costum-form">
                        <button type="button" class="btn costum Yes" id="leadSyn">
                            <span>Yes, I want</span>
                            <span class="consultation">download whitepapers</span>
                        </button>
                        <button class="btn costum No-Thanks" id="leadSyncolse">
                            <span>No, I Want</span>
                            <span class="consultation">spend hours on Google</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div> -->
    <!-- end pop up code -->
    <div class=container>
        <section class="row breath">
            <div class="col-md-12 footerlinks">
                <p>&copy; Copyright 2017 Technology Mindz</p>
            </div>
        </section>
    </div>
    <!--<script type="text/javascript" src=js/jquery.min.js></script>-->
    <!--<script type="text/javascript" src=js/jquery-ui.min.js></script>-->
    <!--<script src=js/jquery.js></script>-->
    <script src=js/bootstrap.min.js></script>
   <!-- <script src=js/ketchup.all.js></script>
    <script src=js/contact_form.js></script>
    <script src=js/exitpopup.js></script>
    <script src=js/script.js></script>
    <script src=js/fitvids.js></script>
    <script>
    jQuery(document).ready(function() {
        jQuery("#videoBlock").fitVids();
    });
    </script> -->
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5aa69492d7591465c7087caf/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
<script>
    function getTname(uaNumber) {
      var _ga = window[window.GoogleAnalyticsObject];
      var trackers = _ga.getAll();
      var i;
      for (i = 0; i < trackers.length; i++) {
        var _tracker = trackers[i];
        if (!uaNumber || _tracker.get('trackingId') === uaNumber) {
          return _tracker.get('name');
        }
      }
    }
    
    
    
    window.addEventListener('load',function(){
     if(window.location.pathname == "/build-mvp/")
     {
    jQuery('.signup').click(function(){
       var clickedCat = jQuery(this).parent('.plan').find('h3:first').html().split('<span')[0].trim();
       
    var gaTrackerName = getTname('UA-58063938-1');
    
    ga(gaTrackerName+'.send','event','free consultation','click',clickedCat);
       
    });
    }
    })

</script>
</body>

</html>
